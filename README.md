## Question

Does NPM registry has any mirror/backup?

## Answer

Yes!

## Usage

1. Create a `.npmrc` file your project and add the following line:

```
registry = https://registry.yarnpkg.com/

-OR-

registry = https://registry.npm.taobao.org/

-OR-

registry = https://r.cnpmjs.org/
```

2. Verify the registry by running `npm config get registry` command

3. Delete your `node_modules` folder and run `npm install` command again

   **Tip**

   You can use `npm --loglevel verbose install` command to detail logs from the installation process

## FYI

Note that the `resolved` keys in `package-lock.json` won't be updated if you are changing from a registry to another. That's [expected behaviour](https://github.com/npm/npm/issues/16849#issuecomment-312442508).
